<?php
/*
 * Titulo      : Pg Dictionary
 * Descripcion : Generador de diccionario de datos PostgreSQL
 * Notas       :
 * Autor       : Amir Torrez - https://gitlab.com/amirtorrez/
 *
 */

/* Parametros de conexion SQL */
define('SQL_HOST', "localhost"); // Servidor SQL
define('SQL_USER', "postgres"); // Usuario SQL
define('SQL_PASS', ""); // Contraseña del usuario SQL
define('SQL_PORT', 5432); // Puerto SQL (5432 default)
define('SQL_DB', ""); // Nombre de la base de datos

define('SQL_SCHEME', "public"); // Esquema a generar el diccionario

// Definir zona horaria para las fechas
date_default_timezone_set("America/Mexico_City");
$scheme = SQL_SCHEME;
$dbname = SQL_DB;

/* Realizar la conexion SQL */
$db['link'] = pg_connect("host=".SQL_HOST." user=".SQL_USER." password=".SQL_PASS." dbname=".SQL_DB." port=".SQL_PORT."");

$selectDBInfo = pg_query($db['link'], "select description from pg_shdescription
join pg_database on objoid = pg_database.oid
where datname = '{$dbname}'");
$resultDB = pg_fetch_array($selectDBInfo);
?>
<!Doctype html>
<html>
  <head>
	<meta charset="utf-8">
    <title>Informe de diccionario de base de Datos - <?php echo SQL_DB;?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0" />
	<style type="text/css">
		body {margin: 0px; padding: 0; }
        td,th {text-align:left;vertical-align:middle}
        table {border-collapse: collapse;border: 1px solid;}
        .caption, th, td {padding: .2em .8em;border: 1px solid #000000;}
        .caption td{background: #000;color: #fff;font-weight: bold;}
        th {font-weight: bold;background: #D3D3D3;color: #000;}
        td {background: #FFFFFF;}
        .center {text-align: center;}
        #ReportHeader { padding:10px; background-color: #fff; color: #111; border-bottom-style: solid; border-bottom-width: 2px; border-color: #222; }
        h1 { font-weight: bold; font-size: 150%; border-bottom-style: solid; border-bottom-width: 2px; margin-top: 0px; padding-bottom: 0.5ex; overflow: hidden; text-overflow: ellipsis; }
    </style>
  </head>
  <body>
<div id="ReportHeader">
	<h1>Informe de diccionario de Base de Datos</h1>
	<b>Generado: </b><?php echo date("d/m/Y H:i:s");?><br />
	<b>Servidor: </b><?php echo SQL_HOST;?> (<?php echo SQL_HOST;?>:<?php echo SQL_PORT;?>)<br />
	<b>Base de Datos: </b><?php echo SQL_DB;?><br />
	<b>Comentario: </b><?php echo $resultDB['description'];?><br />
	<b>Esquema: </b><?php echo $scheme;?><br />
</div><br>
<?php
$selectTables = pg_query($db['link'], "select objoid as id, table_name as table, t3.description as comment
	from information_schema.tables t1
	join pg_class t2 on (table_name = relname)
	left outer join pg_description t3 on (t2.oid = objoid and objsubid = 0)
	where table_schema = '{$scheme}'
	order by table_name");
$resultTables = pg_fetch_all($selectTables);

// conrelid
// usuario: 18914
// paciente: 18902
	foreach($resultTables as $t) {
		$selectConst = pg_query($db['link'], "select conrelid::regclass AS table_from, conname, pg_get_constraintdef(c.oid)
            from   pg_constraint c
            join   pg_namespace n ON n.oid = c.connamespace
            where  conrelid = {$t['id']} order by contype desc");
		$resultConst = pg_fetch_all($selectConst);
		$constNum = pg_num_rows($selectConst);

		$selectColumns = pg_query($db['link'], "select column_name as column, data_type, character_maximum_length as length, is_nullable, column_default, t3.description as comment
            from information_schema.columns t1
            join pg_class t2 on (t1.table_name = t2.relname)
            left outer join pg_description t3 on (t2.oid = t3.objoid and t3.objsubid = t1.ordinal_position)
            where table_schema = '{$scheme}'
              and table_name = '{$t['table']}'
            order by ordinal_position");
		$resultColumns = pg_fetch_all($selectColumns);
?>
<table border="1" align="center" width="99%">
	<thead>
		<tr>
			<th colspan="5" class="center">Tabla: <?php echo "{$t['table']}";?></th>
		</tr>
		<tr>
			<td>Comentario: </td>
			<td colspan="4"><?php echo $t['comment'];?></td>
		</tr>
		<tr>
			<td colspan="5" class="center">Columnas</td>
		</tr>
		<tr class="caption">
			<td class="center">Nombre</td>
			<td class="center">Tipo de dato</td>
			<td class="center">NULL</td>
			<td class="center">Default</td>
			<td class="center">Comentario</td>
		</tr>
	</thead>
	<tbody>
	<?php
	foreach($resultColumns as $c) {
	$c['length'] = (isset($c['length'])) ? "({$c['length']})" : '';
	?>
		<tr>
			<td><?php echo $c['column'];?></td>
			<td><?php echo "{$c['data_type']} {$c['length']}";?></td>
			<td><?php echo $c['is_nullable'];?></td>
			<td><?php echo $c['column_default'];?></td>
			<td><?php echo $c['comment'];?></td>
		</tr>
	<?php
	};
	if($constNum > 0) {
	?>
		<tr>
			<td colspan="5" class="center">Restricciones</td>
		</tr>
		<tr class="caption">
			<td colspan="2" class="center">Nombre</td>
			<td class="center">Tipo</td>
			<td colspan="2" class="center">Definicion</td>
		</tr>
	<?php
		foreach($resultConst as $ct) {
	?>
		<tr>
			<td colspan="2"><?php echo $ct['conname'];?></td>
			<td><?php echo substr($ct['pg_get_constraintdef'], 0,11);?></td>
			<td colspan="2"><?php echo substr($ct['pg_get_constraintdef'], 11);?></td>
		</tr>
	<?php
		};
	};
	?>
	</tbody>
</table>
<br>
<?php
	};
?>
</body>
</html>